<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTangerineIdColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('tangerine_id')->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('valid_id')->nullable();
            $table->string('id_type')->nullable();
            $table->string('id_number')->nullable();
            $table->string('id_url')->nullable();
            $table->string('fingerprint')->nullable();
            $table->string('photo_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('tangerine_id');
            $table->dropColumn('phone_number');
            $table->dropColumn('gender');
            $table->dropColumn('state');
            $table->dropColumn('city');
            $table->dropColumn('address');
            $table->dropColumn('date_of_birth');
            $table->dropColumn('valid_id');
            $table->dropColumn('id_type');
            $table->dropColumn('id_number');
            $table->dropColumn('id_url');
            $table->dropColumn('fingerprint');
            $table->dropColumn('photo_url');
        });
    }
}
